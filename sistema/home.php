<link rel="stylesheet" type="text/css" href="assets/css/inicio.css">

<div class="container">
    <div class="row">
        <div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>AREA DE TRABAJO</p></div>
            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
        </div>
    </div>
    <div>
       <form action="menu.php?id=12" method="post">
           <div class="add">
                <img src="img/mas.png" class="imagen">
                <a data-toggle='collapse' href='#demo' data-toggle="collapse"><h2 class='title-h2'>Agregar Carpeta</h2></a>
           </div>
           <div class="col-md-12 collapse" style="margin-bottom: 15px;" id="demo">
               <div class="col-md-6">
                   <input type="text" name="nombre" class="form-control upper" autofocus="" required="">
               </div>
               <div class="col-md-1" style="margin-top: 5px;">
                    <div class="boton-formulario">
                      <button type="submit" class="boton3">NUEVO DIRECTORIO</button>
                    </div>                   
               </div>
           </div>
       </form> 
    </div>
</div>

<?php

/****************************************************/

//Creamos Nuestra Función
function listFiles($directorio){ //La función recibira como parametro un directorio

    if (is_dir($directorio)) { //Comprobamos que sea un directorio Valido

    if ($dir = opendir($directorio)) {//Abrimos el directorio
     echo "<br>";
    //echo '<ul>'; //Abrimos una lista HTML para mostrar los archivos
     
    while (($archivo = readdir($dir)) !== false){ //Comenzamos a leer archivo por archivo
     
    if ($archivo != '.' && $archivo != '..'){//Omitimos los archivos del sistema . y ..
     
    $nuevaRuta = $directorio.$archivo.'/';//Creamos unaruta con la ruta anterior y el nombre del archivo actual 
     
    //echo '<li>'; //Abrimos un elemento de lista 
    echo "<br>";
    if (is_dir($nuevaRuta)) { //Si la ruta que creamos es un directorio entonces:
        echo '<b>'.$archivo.'***</b>'; //Imprimimos la ruta completa resaltandola en negrita aqui traemos las carpetas y subcarpetas
                    listFiles($nuevaRuta); //Volvemos a llamar a este metodo para que explore ese directorio.

            if(is_dir($archivo)){
                echo $archivo;
                listFiles($archivo);
            }else{

            }



    } else { //si no es un directorio:
     
    //echo $archivo; //simplemente imprimimos el nombre del archivo actual
    echo "<a href=\"$archivo\">$archivo</a><br />";
     
    }
     
    //'</li>'; //Cerramos el item actual y se inicia la llamada al siguiente archivo
     
    }
     
    } //finaliza While
     //echo '</ul>';//Se cierra la lista
     
    closedir($dir); //Se cierra el archivo
    }
    }else{ //Finaliza el If de la linea 12, si no es un directorio valido, muestra el siguiente mensaje
    echo 'No Existe el directorio';
    }
    }//Fin de la Función	
     
    //Llamamos a la función y le pasamos el nombre de nuestro directorio.
    listFiles(__DIR__."/CASOS/");
  

/***************************************************/

/**************************************************/

function obtenerListadoDeArchivos($directorio, $recursivo=false){

    // Array en el que obtendremos los resultados
    $res = array();

    // Agregamos la barra invertida al final en caso de que no exista
    if(substr($directorio, -1) != "/") $directorio .= "/";

    // Creamos un puntero al directorio y obtenemos el listado de archivos
    $dir = @dir($directorio) or die("getFileList: Error abriendo el directorio $directorio para leerlo");
    while(($archivo = $dir->read()) !== false) {
        // Obviamos los archivos ocultos
        if($archivo[0] == ".") continue;
        if(is_dir($directorio . $archivo)) {
            $res[] = array(
                "Nombre" => $directorio . $archivo . "/",
            );
            if($recursivo && is_readable($directorio . $archivo . "/")) {
                $directorioInterior= $directorio . $archivo . "/";
                $res = array_merge($res, obtenerListadoDeArchivos($directorioInterior, true));
            }
        } else if (is_readable($directorio . $archivo)) {
            $res[] = array(
                "Nombre" => $directorio . $archivo,
            );
        }
    }
    $dir->close();
    return $res;
}
//Inicializa variable para collapse
$var = '';
$coll = '';

$carpetas = obtenerListadoDeArchivos(__DIR__."/CASOS//");

foreach ($carpetas as $pCarpeta => $iCarpeta)
{
        foreach ($iCarpeta as $carpeta => $info){
            //echo $info;

            $total_archivos = count(glob($info.'{/*.*}',GLOB_BRACE));
            $var ++;
            $coll ++;

            echo "<div class='wrapper-content' style='max-width: 1110px;'>";
                echo "<div class='wrapper-all-folder'>";

                echo "<div class='item-folder list-group-item'>";
                    echo  $img = "<img src='img/carpeta.png'>";
                    echo "<a data-toggle='collapse' href='#collapse".$var."'><h2 class='title-h2'>".basename($info) ."&nbsp &nbsp($total_archivos-Archivos)</h2></a>";
                    echo $elimina = "<a href='menu.php?id=13&tmp=$info'><img src='img/eliminar.png' style='width: 35px;'>Carpeta</a>";
                    echo $agregar = "<a data-toggle='collapse' href='#colapsar".$coll."'><img src='img/mas.png' style='width: 35px;'>Archivo</a>";
                echo "</div>";

                echo "</div>";

                echo "<form action='menu.php?id=15&rut=$info' method='post' enctype='multipart/form-data'>";
                echo "<div style='margin-bottom: 15px;' id='colapsar".$coll."' class='collapse'>";
                    echo "<input type='file' name='file' class='form-control file'>";
                    echo "<div class='boton-move'>
                            <button type='submit' class='boton3'>Subir Archivo</button>
                        </div> ";
                echo "</div>";
                echo "</form>";
            echo "</div>";

            
            if ($carpeta == 'Nombre'){

                $directorio = opendir($info); //ruta actual

                echo "<div id='collapse".$var."' class='wrapper-doc panel-collapse collapse'>";

                while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
                {
                    if (is_dir($archivo))//verificamos si es o no un directorio
                    {
                        //echo utf8_decode($archivo);
                    }
                    else
                    {

                        
                        $ext = new SplFileInfo($archivo);
                        $tipo =  $ext->getExtension();

                        $archivo = utf8_decode($archivo);

                        if(($tipo == 'zip') or ($tipo == 'rar') ){
                            $img_tipo = "<img class='img-tipo' src='img/winrar.png'>";

                            echo "<div class='wrapper-file'>";
                                echo "<a href='".$info.$archivo."' target='_blank' class='list-group-item'>$img_tipo"." - "."$archivo</a><br>";
                            echo "</div>";                             

                        }elseif (($tipo == 'jpg') or ($tipo == 'png') or ($tipo == 'gif')) {
                            $img_tipo = "<img class='img-tipo' src='img/imagen.png'>";

                            echo "<div class='wrapper-file'>";
                                echo "<a href='".$info.$archivo."' target='_blank' class='list-group-item'>$img_tipo"." - "."$archivo</a><br>";
                            echo "</div>"; 

                        }elseif (($tipo == 'xlsx') or ($tipo == '.xlsm') or ($tipo == 'xls') or ($tipo == 'xltx')) {
                            $img_tipo = "<img class='img-tipo' src='img/excel.png'>";

                            echo "<div class='wrapper-file'>";
                                echo "<a href='".$info.$archivo."' target='_blank' class='list-group-item'>$img_tipo"." - "."$archivo</a><br>";
                            echo "</div>";  

                        }elseif (($tipo == 'docx') or ($tipo == 'txt') or ($tipo == 'doc')) {
                            $img_tipo = "<img class='img-tipo' src='img/doc.png'>";

                            echo "<div class='wrapper-file'>";
                                echo "<a href='".$info.$archivo."' target='_blank' class='list-group-item'>$img_tipo"." - "."$archivo</a><br>";
                            echo "</div>";                             

                        }elseif (($tipo == 'pdf') or ($tipo == 'PDF') ) {
                            $img_tipo = "<img class='img-tipo' src='img/pdf.png'>";

                            echo "<div class='wrapper-file'>";
                                echo "<a href='".$info.$archivo."' target='_blank' class='list-group-item'>$img_tipo"." - "."$archivo</a><br>";
                            echo "</div>";                              

                        }else{

                            echo "<div class='wrapper-file'>";
                                echo "<a href='".$info.$archivo."' target='_blank' class='list-group-item'>$archivo</a><br>";
                            echo "</div>";                              

                        }

  
                    }
                }

                echo "</div>";
            }
        }
}
