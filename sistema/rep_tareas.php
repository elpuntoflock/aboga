<?php
session_start();

$usuario 	= $_SESSION['usuario'];
$usuario 	= strtoupper($usuario);

require_once('db/conexion.php');

$sql = mysqli_query($conn, "SELECT A.ID_CASO, A.DESCRIPCION
										FROM tb_caso A,
											 tb_acceso B
										WHERE A.ID_CASO = B.ID_CASO
										  AND B.ID_USUARIO = '".$usuario."'");

if(isset($_POST['caso']))
    {
      $caso = $_POST['caso'];  
    }
    else{
      $caso = '';        
    }

$tareas = mysqli_query($conn, "SELECT A.ID_TAREA, A.DESCRIPCION, DATE_FORMAT(A.FECHA_CREA,'%d/%m/%Y')FECHA
								FROM tb_tarea A,
									 tb_acceso B
								WHERE A.ID_CASO = B.ID_CASO
								  AND A.ID_CASO = '".$caso."'
								  AND B.ID_USUARIO = '".$usuario."'
								  ORDER BY FECHA ASC");

?>
  <script type="text/javascript">

    function selectItemByValue(elmnt, value){

    ///alert('elmnt: '+elmnt.name +' val: '+ value);

    for(var i=0; i < elmnt.options.length; i++)
      {
        if(elmnt.options[i].value == value)
          elmnt.selectedIndex = i;
      }
    }

  </script>
<div class="container">
	<div class="row">
		<div class="col-md-12">
		<div class="top-line" style="margin-top: 25px; margin-bottom: 30px;">
	        <div class="col-md-4" data-line="movil"><div class="line"></div></div>
	        <div class="col-md-4 titulo-seccion"><p>REPORTE TAREAS</p></div>
	        <div class="col-md-4"><div class="line"></div></div>
	    </div>  			
		</div>	
	</div>
</div>
<form action="menu.php?id=16" method="post">
<div class="container">
	<div class="row">
		<div class="col-md-12">
		<div class="col-md-6">
		<label>Seleccionar Caso</label>
		<select name="caso" class="form-control" id="caso">
			<option>SELECCIONAR CASO</option>
            <?php
            while ($row = mysqli_fetch_array($sql))
            {
              echo '<option value="' . $row['ID_CASO']. '">' . $row['DESCRIPCION'] . '</option>' . "\n";
            }
            ?> 			
		</select>	
		<script language="javascript">
			var numberMI = document.getElementById("caso");
			selectItemByValue(numberMI,<?= "'".$caso."'"?>);
		</script>							
		</div>
		<div class="col-md-1" style="margin-top: 40px;">
		<div class="boton-formulario">
          <button type="submit" class="boton3">BUSCAR</button>
        </div>	
		</div>
        					
		</div>
	</div>
</div>	
</form>

<form>
	<div class="container">
		<div class="row">

		    <div class="col-md-12 table-responsive" style="margin-top: 75px;">
		      <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
		          <thead>
		              <tr>
		                  <th class="centrar">ID TAREA</th>
		                  <th class="centrar">DESCRIPCI&Oacute;N DEL CASO</th>
		                  <th class="centrar">FECHA</th>
		              </tr>
		          </thead>
		          <tbody>
		          <?php
		      		while ($row = mysqli_fetch_array($tareas)){

			          echo "<tr>";
			          echo "<td>$row[0]</td>";
			          echo "<td style='text-align: left;'>$row[1]</td>";
			          echo "<td style='text-align: center;'>$row[2]</td>";
			        echo "</tr>";
			        } 
		      	?>       
		          </tbody>
		      </table>
		    </div>

		</div>
	</div>
</form>
