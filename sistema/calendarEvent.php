<?php
session_start();

$usuario 	= $_SESSION['usuario'];
$usuario 	= strtoupper($usuario);
//CEM 20180507 Modificar para utilizar variables de conexión 
try
{
	$bdd = new PDO('mysql:host=mysql.hostinger.com;
								dbname=u976859431_aboga;
								charset=utf8', 
								'u976859431_aboga', 
								'0Bk7w7f6yZVt');
/*								
	$bdd = new PDO('mysql:host=localhost;
									dbname=admin;
									charset=utf8',
									'u976859431_aboga',
									'0Bk7w7f6yZVt');*/
}
catch(Exception $e)
{
        die('Error : '.$e->getMessage());
}


$sql = "SELECT A.ID_TAREA, A.DESCRIPCION, A.OBSERVACIONES, A.FECHA_INI, A.FECHA_FIN, A.COLOR
		FROM tb_tarea A,
			 tb_acceso B
		WHERE A.ID_CASO = B.ID_CASO
		  AND A.ESTATUS = 'A'
		  AND B.ID_USUARIO = '".$usuario."'";

$req = $bdd->prepare($sql);
$req->execute();

$events = $req->fetchAll();


?>

<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='fullCalendar/css/fullcalendar.css' rel='stylesheet' />


    <!-- Custom CSS -->
    <style>
    #calendar {
		max-width: 1110px;
	}
	.col-centered{
		float: none;
		margin: 0 auto;
	}

	.form-control {
	height: 55px;
	font-size: 20px;
	color: #99422F;
	}

	.modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
}
    </style>



</head>

<body>
    <div class="container">

        <div class="row">
            <div class="col-lg-12 text-center">
                <div id="calendar" class="col-centered">
                </div>
            </div>
			
        </div>
        <!-- /.row -->
		
		<!-- Modal -->
		<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<form class="form-horizontal" method="POST" action="fullCalendar/addEvent.php">
			
			  <div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">INGRESO DE NUEVAS TAREAS</h4>
			  </div>
			  <div class="modal-body">
            <div>
              <label>SELECCIONAR NUMERO DE CASO</label>
              <select name="id_caso" class="form-control abono">
                <option value="-1">SELECCIONAR CASO</option>
            <?php

            require_once('db/conexion.php');

			$sql = mysqli_query($conn, "SELECT A.ID_CASO, A.DESCRIPCION
																		FROM tb_caso A,
											 									 tb_acceso B
																	  WHERE A.ID_CASO = B.ID_CASO
										  								AND B.ID_USUARIO = '".$usuario."'");


            while ($row = mysqli_fetch_array($sql))
            {
              echo '<option value="' . $row['ID_CASO']. '">' . $row['DESCRIPCION'] . '</option>' . "\n";
            }
            ?>                  
              </select>
            </div>

            <div>
            	<label>DESCRIPCI&Oacute;N</label>
            	<input type="text" name="descripcion" class="form-control upper" placeholder="Ingresar Descripci&oacute;n" required="">
            </div>

            <div>
            	<label>OBSERVACIONES</label>
				<input type="text" name="observaciones" class="form-control upper" placeholder="OBSERVACIONES">
            </div>		  	
				  <div>
					<label>Color</label>

					  <select name="color" class="form-control" id="color">
						  <option value="">Seleccionar</option>
						  <option style="color:#0071c5;" value="#0071c5">&#9724; Azul oscuro</option>
						  <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquesa</option>
						  <option style="color:#008000;" value="#008000">&#9724; Verde</option>
						  <option style="color:#FFD700;" value="#FFD700">&#9724; Amarillo</option>
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; Naranja</option>
						  <option style="color:#FF0000;" value="#FF0000">&#9724; Rojo</option>
						  <option style="color:#000;" value="#000">&#9724; Negro</option>
						  
						</select>
				  </div>

				<div>
					<label>FECHA INICIO</label>
					<input type="text" name="fecha_inicio" class="form-control centrar" id="start" placeholder="FECHA INICIO" required="">
				</div>

				<div>
					<label>FECHA FINAL</label>
					<input type="text" name="fecha_final" class="form-control centrar" id="end" placeholder="FECHA FINAL" required="">
				</div>

				<div>
						<label for="">NOTIFICACI&Oacute;N</label>
						<input type="text" name="notificacion" class="form-control centrar upper" placeholder="NOTIFICACI&Oacute;N" require="">
				</div>
				
				<div style="margin-top: 15px;">
						<label for="">NOTIFICAR EMAIL # 1</label>
						<select name="email1" id="" class="form-control centrar">
							<option value="">NOTIFICAR EMAIL # 1</option>
							<?php

								require_once('db/conexion.php');

								$email1 = mysqli_query($conn, "SELECT ID_CONTACTO, EMAIL
																							FROM tb_contacto
																							WHERE EMAIL IS NOT NULL");
								while ($rowX = mysqli_fetch_array($email1))
								{
									echo '<option value="' . $rowX['EMAIL']. '">' . $rowX['EMAIL'] . '</option>' . "\n";
								}
								?> 							
						</select>

				<div style="text-align: left;">
					<a data-toggle='collapse' href='#demo' data-toggle="collapse"><img src="img/mas.png" width="25px"><img src="img/correo.png" width="35px"></a>
				</div>

				</div>

				<div id="demo" class="collapse">
						<label for="">NOTIFICAR EMAIL # 2</label>
						<select name="email2" id="" class="form-control">
								<option value="">NOTIFICAR EMAIL # 2</option>
								<?php

								require_once('db/conexion.php');

								$email2 = mysqli_query($conn, "SELECT ID_CONTACTO, EMAIL
																							FROM tb_contacto
																							WHERE EMAIL IS NOT NULL");
								while ($rowX1 = mysqli_fetch_array($email2))
								{
									echo '<option value="' . $rowX1['EMAIL']. '">' . $rowX1['EMAIL'] . '</option>' . "\n";
								}
								?> 								
						</select>

				<div style="text-align: left;">
					<a data-toggle='collapse' href='#email' data-toggle="collapse"><img src="img/mas.png" width="25px"><img src="img/correo.png" width="35px"></a>
				</div>

				</div>

				<div id="email" class="collapse">
						<label for="">NOTIFICAR EMAIL # 3</label>
						<select name="email3" class="form-control">
								<option value="">NOTIFICAR EMAIL # 3</option>
								<?php

								require_once('db/conexion.php');

								$email3 = mysqli_query($conn, "SELECT ID_CONTACTO, EMAIL
																							FROM tb_contacto
																							WHERE EMAIL IS NOT NULL");
								while ($rowX2 = mysqli_fetch_array($email3))
								{
									echo '<option value="' . $rowX2['EMAIL']. '">' . $rowX2['EMAIL'] . '</option>' . "\n";
								}
								?> 									
						</select>
				</div>

				<div>
				   <label for="">FECHA AVISO</label>
					 <input type="text" name="fec_aviso" id="fec_notifica" class="form-control centrar upper" placeholder="Ingresar Fecha de Notificaci&oacute;n" require="">
				</div>	

				<div>
						<label for="">DIAS PREVIOS AL AVISO:</label>
							<select name="dias" class="form-control">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
							</select>
				</div>			


			
			  </div>
			  <div class="modal-footer">
				<button type="button" class="boton_close" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="boton3">Guardar</button>
			  </div>
			</form>
			</div>
		  </div>
		</div>
		
		<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<form class="form-horizontal" method="POST" action="fullCalendar/editEventTitle.php">
			  <div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">MODIFICAR TAREA</h4>
			  </div>
			  <div class="modal-body">

			  	<div>
			  		<label>CASO</label>
			  		<input type="text" name="id" class="form-control" id="id" readonly="">
			  	</div>
				
				  <div>
					<label>DESCRIPCI&Oacute;N</label>
					  <input type="text" name="title" class="form-control" id="title" placeholder="Titulo">
				  </div>

  	            <div>
	            	<label>OBSERVACIONES</label>
					<input type="text" name="observaciones" id="observaciones" class="form-control upper" placeholder="OBSERVACIONES">
	            </div>	

				  <div>
					<label>Color</label>
					  <select name="color" class="form-control" id="color">
						  <option value="">Seleccionar</option>
						  <option style="color:#0071c5;" value="#0071c5">&#9724; Azul oscuro</option>
						  <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquesa</option>
						  <option style="color:#008000;" value="#008000">&#9724; Verde</option>
						  <option style="color:#FFD700;" value="#FFD700">&#9724; Amarillo</option>
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; Naranja</option>
						  <option style="color:#FF0000;" value="#FF0000">&#9724; Rojo</option>
						  <option style="color:#000;" value="#000">&#9724; Negro</option>
						</select>

				  </div>
				    <div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-10">
						  <div class="checkbox">
							<label class="text-danger"><input type="checkbox"  name="delete"> Eliminar Evento</label>
						  </div>
						</div>
					</div>
				  
				  <input type="hidden" name="id" class="form-control" id="id">
				
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="boton_close" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="boton3">Guardar</button>
			  </div>
			</form>
			</div>
		  </div>
		</div>

    </div>
    <script src="fullCalendar/js/jquery.js"></script>
	<script src='fullCalendar/js/moment.min.js'></script>
	<script src='fullCalendar/js/fullcalendar/fullcalendar.js'></script>
	
	<script>

	$(document).ready(function() {

	   var date = new Date();
       var yyyy = date.getFullYear().toString();
       var mm = (date.getMonth()+1).toString().length == 1 ? "0"+(date.getMonth()+1).toString() : (date.getMonth()+1).toString();
       var dd  = (date.getDate()).toString().length == 1 ? "0"+(date.getDate()).toString() : (date.getDate()).toString();
		
		$('#calendar').fullCalendar({
			header: {
				language: 'es',
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay',

			},
			defaultDate: yyyy+"-"+mm+"-"+dd,
			editable: true,
			eventLimit: true,
			selectable: true,
			selectHelper: true,
			select: function(start, end) {
				
				$('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
				$('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
				$('#ModalAdd').modal('show');
			},
			eventRender: function(event, element) {
				element.bind('dblclick', function() {
					$('#ModalEdit #id').val(event.id);
					$('#ModalEdit #title').val(event.title);
					$('#ModalEdit #observaciones').val(event.observaciones);
					$('#ModalEdit #color').val(event.color);
					$('#ModalEdit').modal('show');
				});
			},
			eventDrop: function(event, delta, revertFunc) { // si changement de position

				edit(event);

			},
			eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur

				edit(event);

			},
			events: [
			<?php 
			foreach($events as $event): 
			
				$start = explode(" ", $event['FECHA_INI']);
				$end = explode(" ", $event['FECHA_FIN']);
				if($start[1] == '00:00:00'){
					$start = $start[0];
				}else{
					$start = $event['start'];
				}
				if($end[1] == '00:00:00'){
					$end = $end[0];
				}else{
					$end = $event['end'];
				}
			?>
				{
					id: 			'<?php echo $event['ID_TAREA']; ?>',
					title: 			'<?php echo $event['DESCRIPCION']; ?>',
					observaciones: 	'<?php echo $event['OBSERVACIONES']; ?>',
					start: 			'<?php echo $start; ?>',
					end: 			'<?php echo $end; ?>',
					color: 			'<?php echo $event['COLOR']; ?>',
				},
			<?php endforeach; ?>
			]
		});
		
		function edit(event){
			start = event.start.format('YYYY-MM-DD HH:mm:ss');
			if(event.end){
				end = event.end.format('YYYY-MM-DD HH:mm:ss');
			}else{
				end = start;
			}
			
			id =  event.id;
			
			Event = [];
			Event[0] = id;
			Event[1] = start;
			Event[2] = end;
			
			$.ajax({
			 url: 'fullCalendar/editEventDate.php',
			 type: "POST",
			 data: {Event:Event},
			 success: function(rep) {
					if(rep == 'OK'){
						alert('Evento se ha guardado correctamente');
					}else{
						alert('No se pudo guardar. Inténtalo de nuevo.'); 
					}
				}
			});
		}
		
	});

</script>
</body>
</html>
