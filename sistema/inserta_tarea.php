<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
    <script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
function Error()
    {
      swal({title:"Error al Insertar!", type:"error", showConfirmButton:false, text:"OCURRIO ALGUN ERROR!...", timer:'2000'}, 

      function () 
    {
      location.href = "menu.php?id=5"; 
    });
    }

function Ingresado()
    {
      swal({title:"Tarea Grabada con Exito!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=5"; 
    });
    }

</script>		
	<title></title>
</head>
<body>
<?php
session_start();
require_once('db/conexion.php');

$sql = mysqli_query($conn, "SELECT MAX(ID_TAREA)+1 CONTEO
							FROM tb_tarea");
while($row = $sql->fetch_array(MYSQLI_ASSOC)){
    $id_tarea = $row['CONTEO'];

    if($id_tarea == 0){
    	$id_tarea = 1;
    }else{
    	$id_tarea = $row['CONTEO'];
    }
}

//$id_tarea = "1";

$usuario 		= $_SESSION['usuario'];
$usuario 		= strtoupper($usuario);
$id_caso		= $_POST['caso'];
$newDate		= $_POST['fecha_inicio'];
$fecha_inicio 	= date("Y/m/d", strtotime($newDate));	

$newDate_2		= $_POST['fecha_final'];
$fecha_final 	= date("Y/m/d", strtotime($newDate_2));
$descripcion 	= strtoupper($_POST['descripcion']);
$observaciones	= strtoupper($_POST['observaciones']);
$estatus 		= "A";

$insert 	= mysqli_query($conn, "INSERT INTO tb_tarea (
									ID_TAREA,
									ID_CASO,
									DESCRIPCION,
									FECHA_INI,
									FECHA_FIN,
									USUARIO_CREA,
									FECHA_CREA,
									ESTATUS,
									OBSERVACIONES)VALUES(
									'".$id_tarea."',
									'".$id_caso."',
									'".$descripcion."',
									'".$fecha_inicio."',
									'".$fecha_final."',
									'".$usuario."',
									CURRENT_TIMESTAMP,
									'".$estatus."',
									'".$observaciones."')");

if($insert == TRUE){
	echo "<script>Ingresado();</script>";
}else{
	echo "<script>Error();</script>";
	//echo "Error: " . $insert . "<br>" . $conn->error;
}

$conn->close();		

?>
</body>
</html>