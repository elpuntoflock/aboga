<?php
require_once('db/conexion.php');

$sql = mysqli_query($conn, "SELECT NOMBRES, APELLIDOS, CUI, TELEFONO, DIRECCION, ZONA, EMAIL, ID_CONTACTO
							FROM tb_contacto");

?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

   <form action="inserta_cliente.php" method="post">

	<div class="contenido">
		
		<div class="col-md-12">
		<div class="top-line" style="margin-top: 25px; margin-bottom: 30px;">
	        <div class="col-md-4" data-line="movil"><div class="line"></div></div>
	        <div class="col-md-4 titulo-seccion"><p>INGRESA CLIENTE</p></div>
	        <div class="col-md-4"><div class="line"></div></div>
	    </div>  			
		</div>	
		

		<div class="col-md-12 bajar">
			<div class="col-md-4">
				<label>NOMBRES</label>
				<input type="text" name="nombres" class="form-control upper" placeholder="Ingresar Nombres" autofocus="" required="">				
			</div>
			<div class="col-md-4">
				<label>APELLIDOS</label>
				<input type="text" name="apellidos" class="form-control upper" placeholder="Ingresar Apellidos" required="">
			</div>
			<div class="col-md-4">
				<label>APELLIDO CASADA</label>
				<input type="text" name="apellido_casada" class="form-control upper" placeholder="Apellido Casada">
			</div>			
		</div>

		<div class="col-md-12 bajar">
			<div class="col-md-4">
				<label>CUI</label>
				<input type="text" name="cui" class="form-control centrar upper" placeholder="Ingresar Cui" onkeypress="return valida(event)" maxlength="13" minlength="13" required="">
			</div>
			<div class="col-md-4">
				<label>PASAPORTE</label>
				<input type="text" name="pasaporte" class="form-control centrar upper" placeholder="Ingresar Pasaporte">
			</div>
			<div class="col-md-4">
				<label>TELEFONO</label>
				<input type="text" name="telefono" class="form-control centrar upper" placeholder="Ingresar Telefono" onkeypress="return valida(event)" maxlength="8" minlength="8" required="">
			</div>						
		</div>

		<div class="col-md-12 bajar">
			<div class="col-md-3">
				<label>SEXO</label>
				<select name="sexo" class="form-control">
					<option value="M">MASCULINO</option>
					<option value="F">FEMENINO</option>
				</select>
			</div>
			<div class="col-md-7">
				<label>DIRECCI&Oacute;N</label>
				<input type="text" name="direccion" class="form-control upper" placeholder="Direcci&oacute;n" required="">
			</div>
			<div class="col-md-2">
				<label>ZONA</label>
				<select name="zona" class="form-control" required="">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="21">21</option>
					<option value="24">24</option>
					<option value="25">25</option>
				</select>
			</div>
		</div>
		<div class="col-md-12 bajar">
			<div class="col-md-4">
				<label>PAIS</label>
				<select name="pais" class="form-control" id="pais" required="">
					<option></option>
				</select>
			</div>
			<div class="col-md-4">
				<label>DEPARTAMENTO</label>
				<select name="departamento" class="form-control" id="departamento" required="">
					<option>SELECCIONAR DEPARTAMENTO</option>
				</select>
			</div>		
			<div class="col-md-4">
				<label>MUNICIPIO</label>
				<select name="municipio" class="form-control" id="municipio" required="">
					<option>SELECCIONAR MUNICIPIO</option>
				</select>
			</div>				
		</div>

		<div class="col-md-12 bajar">
			<div class="boton-formulario">
			<center>
				<button type="submit" class="boton3">GRABAR</button>
			</center>
			</div>			
		</div>

	</div>
</form>

    <div class="container bajar">
        <div class="row">
        <div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>TABLA CONTACTOS</p></div>
            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
        </div>        
    <div class="col-md-12 table-responsive bajar">
	    <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
	        <thead>
	            <tr>
	                <th class="centrar">NOMBRES</th>
	                <th class="centrar">APELLIDOS</th>
	                <th class="centrar">CUI</th>
	                <th class="centrar">TELEFONO</th>
					<th class="centrar">EDITAR CONTACTO</th>
	            </tr>
	        </thead>
	        <tbody>
	       	<?php
			while ($row = mysqli_fetch_array($sql)){
				echo "<tr>";
					$id_contacto 	= $row[7];
					$nombres 		= $row[0];
					$apellidos		= $row[1]; 
					$telefono 		= $row[3];
					$direccion		= $row[4];
					$zona 			= $row[5];
					$email 			= $row[6];

					echo "<td>$row[0]</td>";
					echo "<td>$row[1]</td>";
					echo "<td>$row[2]</td>";
					echo "<td>$row[3]</td>";
					echo "<td><a href='#' data-toggle='modal' data-target='#myModal' 
					data-id_contact	= '$id_contacto'
					data-nombres 	= '$nombres'
					data-apellidos 	= '$apellidos'
					data-direccion	= '$direccion'
					data-zona		= '$zona'
					data-telefono	= '$telefono'
					data-email 		= '$email'><img class='img-edit' src='img/edit.png'></a></td>";
				echo "</tr>";
				} 
			?>           
	        </tbody>
	    </table>
    </div>
        </div>
    </div>	



<script type="text/javascript">
		  /*Funcion de Validar solo numeros*/
    function valida(e){
      tecla = (document.all) ? e.keyCode : e.which;

      if (tecla==8){
          return true;
      }
      patron =/[0-9]/;
      tecla_final = String.fromCharCode(tecla);
      return patron.test(tecla_final);
  	}
</script>

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
          <h4 class="modal-title">MODIFICACI&Oacute;N CONTACTOS</h4>
        </div>
		
		<div class="modal-body">

		<form action="edit_contacto.php" method="post">
			<div class="ocultar">
				<label>ID CONTACTO</label>
				<input type="text" name="id" class="form-control centrar" readonly="">
			</div>
			<div>
	  			<label>NOMBRES</label>
	  			<input type="text" name="nom_contacto" class="form-control upper" placeholder="Nombres">
		  </div>
		  <div>
	  			<label>APELLIDOS</label>
				<input type="text" name="ape_contacto" class="form-control upper" placeholder="Apellidos">
		  </div>
		  <div>
	  			<label>DIRECCI&Oacute;N</label>
				<input type="text" name="address" class="form-control upper" placeholder="Direcci&oacute;n">
		  </div>
		  <div>
	  			<label>ZONA</label>
				  <select name="zona_contacto" class="form-control upper">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="21">21</option>
					<option value="24">24</option>
					<option value="25">25</option>
				</select>
		  </div>
		  <div>
	  			<label>TELEFONO</label>
	  			<input type="text" name="tel_contacto" class="form-control upper" placeholder="Telefono">
		  </div>
		  <div>
	  			<label>EMAIL</label>
				<input type="text" name="email_contacto" class="form-control" placeholder="Correo Electronico">
		  </div>			
        </div>
		
        <div class="modal-footer">
			<button type="submit" class="boton3">GRABAR</button>
        	<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
        </div>	
		</form>
		</div>	
		
   
    </div>
  </div>
  <script>
	$('#myModal').on('show.bs.modal', function(e)
	{
		var id 			= $(e.relatedTarget).data('id_contact');
		var nombres 	= $(e.relatedTarget).data('nombres');
		var apellidos 	= $(e.relatedTarget).data('apellidos');
		var direccion 	= $(e.relatedTarget).data('direccion');
		var zona	 	= $(e.relatedTarget).data('zona');
		var telefono 	= $(e.relatedTarget).data('telefono');
		var email	 	= $(e.relatedTarget).data('email');

		$(e.currentTarget).find('input[name="id"]').val(id);
		$(e.currentTarget).find('input[name="nom_contacto"]').val(nombres);
		$(e.currentTarget).find('input[name="ape_contacto"]').val(apellidos);
		$(e.currentTarget).find('input[name="address"]').val(direccion);
		$(e.currentTarget).find('select[name="zona_contacto"]').val(zona);
		$(e.currentTarget).find('input[name="tel_contacto"]').val(telefono);
		$(e.currentTarget).find('input[name="email_contacto"]').val(email);
	});
  </script>