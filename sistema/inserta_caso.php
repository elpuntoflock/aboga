<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
    <script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
function Error()
    {
      swal({title:"Error al Insertar!", type:"error", showConfirmButton:false, text:"OCURRIO ALGUN ERROR!...", timer:'2000'}, 

      function () 
    {
      location.href = "menu.php?id=2"; 
    });
    }

function Ingresado()
    {
      swal({title:"Caso Grabado con Exito!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=2"; 
    });
    }

</script>	
	<title></title>
</head>
<body>
<?php
require_once('db/conexion.php');
session_start();

$usuario_crea 	= $_SESSION['usuario'];
$usuario_crea 	= strtoupper($usuario_crea);

$id_contacto		= $_POST['id_contacto'];
$area			= $_POST['area'];
$descripcion	= strtoupper($_POST['descripcion']);
$newDate		= $_POST['fec_inicio'];
$fecha_inicio 	= date("Y/m/d", strtotime($newDate));	

$newDate_2		= $_POST['fec_final'];
$fecha_final 	= date("Y/m/d", strtotime($newDate_2));
	
$observacion	= strtoupper($_POST['text']);

$ruta = $_POST['ruta'];


$sql = mysqli_query($conn, "SELECT MAX(ID_CASO)+1 CONTEO
							FROM tb_caso");
while($row = $sql->fetch_array(MYSQLI_ASSOC)){
    $id_caso = $row['CONTEO'];

    if($id_caso == 0){
    	$id_caso = 1;
    }else{
    	$id_caso = $row['CONTEO'];
    }
        
}

/*************************************/
$carpeta = $ruta.'\\'.$descripcion;

if (!file_exists($carpeta)) {
	mkdir($carpeta, 0777, true);
}else{
	
}

/*************************************/
$archivo = (isset($_FILES['file'])) ? $_FILES['file'] : null;
if ($archivo) {
   $ruta_destino_archivo = $ruta.'\\'.$descripcion.'\\'."{$archivo['name']}";
   $archivo_ok = move_uploaded_file($archivo['tmp_name'], $ruta_destino_archivo);
}
/************************************/

$insert = mysqli_query($conn, "INSERT INTO tb_caso (
								ID_CASO,
								ID_CONTACTO,
								ID_AREA,
								DESCRIPCION,
								FECHA_INI,
								FECHA_FIN,
								USUARIO_CREA,
								FECHA_CREA,
								OBSERVACIONES)VALUES(
								'".$id_caso."',
								'".$id_contacto."',
								'".$area."',
								'".$descripcion."',
								'".$fecha_inicio."',
								'".$fecha_final."',
								'".$usuario_crea."',
								CURRENT_TIMESTAMP,
								'".$observacion."')");

if($insert == TRUE){
	echo "<script>Ingresado();</script>";
}else{
	echo "<script>Error();</script>";
	//echo "Error: " . $insert . "<br>" . $conn->error;
}	

?>
</body>
</html>