<?php
require_once('db/conexion.php');

$sql = mysqli_query($conn, "SELECT ID_CASO, DESCRIPCION
								FROM tb_caso");

$tarea = mysqli_query($conn, "SELECT ID_TAREA, DESCRIPCION, DATE_FORMAT(FECHA_INI,'%d/%m/%Y'), DATE_FORMAT(FECHA_FIN,'%d/%m/%Y'), ESTATUS
								FROM tb_tarea");

?>
<form action="inserta_tarea.php" method="post">
	<div class="container">
		<div class="row">
				<div class="col-md-12">
				<div class="top-line" style="margin-top: 25px; margin-bottom: 30px;">
			        <div class="col-md-4" data-line="movil"><div class="line"></div></div>
			        <div class="col-md-4 titulo-seccion"><p>INGRESO TAREAS</p></div>
			        <div class="col-md-4"><div class="line"></div></div>
			    </div>  			
				</div>		
			<div class="col-md-12">
				<div class="col-md-5">
					<label>SELECCIONAR CASO</label>
					<select name="caso" class="form-control" autofocus="">
						<option value="0">SELECCIONAR CASO</option>
						<?php
						while ($row = mysqli_fetch_array($sql))
						{
							echo '<option value="' . $row['ID_CASO']. '">' . $row['DESCRIPCION'] . '</option>' . "\n";
						}
						?>					
					</select>
				</div>

				<div class="col-md-3">
					<label>FECHA INICIO</label>
					<input type="text" name="fecha_inicio" class="form-control centrar" id="datepicker" placeholder="FECHA INICIO">
				</div>
				<div class="col-md-3">
					<label>FECHA FINAL</label>
					<input type="text" name="fecha_final" class="form-control centrar" id="datepicker_1" placeholder="FECHA FINAL">
				</div>			

			</div>

			<div class="col-md-12 bajar">
				<div class="col-md-6">
					<label>DESCRIPCION</label>
					<input type="text" name="descripcion" class="form-control upper" placeholder="Ingresar Descripci&oacute;n">
				</div>
				<div class="col-md-6">
					<label>OBSERVACIONES</label>
					<input type="text" name="observaciones" class="form-control" placeholder="OBSERVACIONES">
				</div>
			</div>

			<div class="col-md-12 bajar">
				<div class="boton-formulario">
				<center>
					<button type="submit" class="boton3">GRABAR</button>
				</center>
				</div>			
			</div>		
		</div>
	</div>	
</form>
    <div class="container bajar">
    	<a href="img/edit.png"></a>
        <div class="row">
        <div class="top-line" style="margin-top: 25px; margin-bottom: 30px;">
            <div class="col-md-4" data-line="movil"><div class="line"></div></div>
            <div class="col-md-4 titulo-seccion bajar_espacio"><p>TABLA DE CASOS</p></div>
            <div class="col-md-4"><div class="line"></div></div>
        </div>        
            <div class="col-md-12 table-responsive bajar">
            <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
        <thead>
            <tr>
                <th class="centrar">TAREA</th>
                <th class="centrar">DESCRIPCION</th>
                <th class="centrar">FECHA INICIO</th>
                <th class="centrar">FECHA FINAL</th>
                <th class="centrar">ESTATUS</th>
                <th class="centrar">MODIFICAR</th>
            </tr>
        </thead>
        <tbody>
	       	<?php
			while ($res = mysqli_fetch_array($tarea)){
				echo "<tr>";
					echo "<td>$res[0]</td>";
					echo "<td>$res[1]</td>";
					echo "<td>$res[2]</td>";
					echo "<td>$res[3]</td>";
					echo "<td>$res[4]</td>";
					echo "<td><button type='button' class='btn btn-info' data-toggle='modal' data-target='#myModal'>X</button></td>";
				echo "</tr>";
				} 
			?>           
        </tbody>
    </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
          <h4 class="modal-title">MODIFICA TAREA</h4>
        </div>
        <form action="" method="post">
        <div class="modal-body">
        	<div>
				<label>DESCRIPCI&Oacute;N</label>
				<input type="text" name="descri" class="form-control upper" placeholder="INGRESAR DESCRIPCI&Oacute;N">
        	</div>
			<div class="baja">
				<label>FECHA INICIO</label>
				<input type="text" name="fecha1" id="fecha1" class="form-control centrar" placeholder="FECHA INICIO">	
			</div>
			<div class="baja">
				<label>FECHA FINAL</label>
				<input type="text" name="fecha2" id="fecha2" class="form-control centrar" placeholder="FECHA FINAL">	
			</div>
			<div>
				<label>OBSERVACIONES</label>
				<input type="text" name="observa" class="form-control uppper" placeholder="INGRESAR OBSERVACIONES">
			</div>			
        </div>
        <div class="modal-footer">
        	<button type="button" class="boton3" data-dismiss="modal">GRABAR</button>
          <button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
        </div>        	
        </form>

      </div>
      
    </div>
  </div>