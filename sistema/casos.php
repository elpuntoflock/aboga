<?php
require_once('db/conexion.php');

$contacto = mysqli_query($conn,"SELECT ID_CONTACTO, CONCAT(NOMBRES,' ',APELLIDOS)NOMBRES
								FROM tb_contacto");

$area = mysqli_query($conn,"SELECT ID_AREA, DESCRIPCION
								FROM tb_area");

$busqueda	= mysqli_query($conn, "SELECT CONCAT(B.NOMBRES,' ',B.APELLIDOS)NOMBRES, C.DESCRIPCION, A.DESCRIPCION, DATE_FORMAT(A.FECHA_INI,'%d/%m/%Y'), DATE_FORMAT(A.FECHA_FIN,'%d/%m/%Y'), 
									A.ID_CONTACTO, A.ID_CASO, A.ID_AREA, A.OBSERVACIONES
									FROM 	tb_caso A,
	 										tb_contacto B,
     										tb_area C
 									WHERE A.ID_CONTACTO = B.ID_CONTACTO
										 AND A.ID_AREA	 = C.ID_AREA");
										 
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<form action="inserta_caso.php" method="post" enctype="multipart/form-data">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			<div class="top-line" style="margin-top: 25px; margin-bottom: 30px;">
		        <div class="col-md-4" data-line="movil"><div class="line"></div></div>
		        <div class="col-md-4 titulo-seccion"><p>INGRESO DE CASOS</p></div>
		        <div class="col-md-4"><div class="line"></div></div>
		    </div>  			
			</div>		
				
			<div class="col-md-12">
				<!--div class="col-md-2"></div-->
				<div class="col-md-4">
					<label>CONTACTO</label>
					<select name="id_contacto" class="form-control" autofocus="">
						<option value="">SELECCIONAR CONTACTO</option>
					<?php
					while ($row = mysqli_fetch_array($contacto))
					{
						echo '<option value="' . $row['ID_CONTACTO']. '">' . $row['NOMBRES'] . '</option>' . "\n";
					}
					?>
					</select>	
				</div>

				<div class="col-md-4">
					<label>AREA</label>
					<select name="area" class="form-control">
						<option value="">SELECCIONAR AREA</option>
					<?php
					while ($row_X = mysqli_fetch_array($area))
					{
						echo '<option value="' . $row_X['ID_AREA']. '">' . $row_X['DESCRIPCION'] . '</option>' . "\n";
					}
					?>
					</select>	
				</div>	


			<div class="col-md-4">
			<?php
								echo "<label for=''>RUTA DE CASO</label>";
								echo "<select name='ruta' id='ruta' class='form-control upper'>";

				function listar_archivos($carpeta){
					if(is_dir($carpeta)){
						if($dir = opendir($carpeta)){
							while(($archivo = readdir($dir)) !== false){
								if($archivo != '.' && $archivo != '..' && $archivo != '.htaccess'){
									//echo '<li><a target="_blank" href="'.$carpeta.'/'.$archivo.'">'.$archivo.'</a></li>';
									
									echo "<option value='$carpeta/$archivo'>$archivo</option>";
								}
							}
							closedir($dir);
						}
					}
					}
				
					echo listar_archivos(__DIR__."/CASOS");	


						
					echo "</select>";
			?>
			</div>

			</div>

			<div class="col-md-12 bajar">
				<div class="col-md-6">
					<label>DESCRIPCION</label>
					<input type="text" name="descripcion" class="form-control upper" placeholder="DESCRIPCI&Oacute;N DEL CASO" required="">
				</div>
				<div class="col-md-3">
					<label>FECHA INICIO</label>
					<input type="text" name="fec_inicio" class="form-control centrar" id="datepicker" placeholder="FECHA INICIO" required="">
				</div>
				<div class="col-md-3">
					<label>FECHA FINAL</label>
					<input type="text" name="fec_final" class="form-control centrar" id="datepicker_1" placeholder="FECHA FINAL" required="">
				</div>				
			</div>
			<div class="col-md-12 bajar">
				<label>OBSERVACIONES</label>
				<input type="text" name="text" class="form-control upper" placeholder="OBSERVACIONES">
			</div>
			
			<div class="col-md-12 bajar">
					<div class="col-md-6">
						<label for="">Adjuntar archivo</label>
						<input type="file" name="file" class="form-control">
					</div>
			</div>

		<div class="col-md-12 bajar">
			<div class="boton-formulario">
			<center>
				<button type="submit" class="boton3">GRABAR</button>
			</center>
			</div>			
		</div>			
		</div>

    <div class="container bajar">
        <div class="row">
        <div class="top-line" style="margin-top: 25px; margin-bottom: 30px;">
            <div class="col-md-4" data-line="movil"><div class="line"></div></div>
            <div class="col-md-4 titulo-seccion bajar_espacio"><p>TABLA DE CASOS</p></div>
            <div class="col-md-4"><div class="line"></div></div>
        </div>        
            <div class="col-md-12 table-responsive bajar">
            <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
        <thead>
            <tr>
                <th class="centrar">CONTACTO</th>
                <th class="centrar">AREA</th>
                <th class="centrar">DESCRIPCI&Oacute;N</th>
                <th class="centrar">FECHA INICIO</th>
                <th class="centrar">FECHA FINAL</th>
				<th class="centrar">EDITAR</th>
            </tr>
        </thead>
        <tbody>
	       	<?php
			while ($res = mysqli_fetch_array($busqueda)){
				$detalle = "<img src='img/detalle.png' width='35px'>";
				echo "<tr>";
					$id_caso 		= $res[6];
					$id_cliente		= $res[5];
					$id_area		= $res[7];
					$descripcion	= $res[2];
					$fec_inicio		= $res[3];
					$fec_final		= $res[4];
					$observaciones	= $res[8];
					echo "<td>$res[0]</td>";
					echo "<td width='30%'>$res[1]</td>";
					echo "<td>$res[2]</td>";
					echo "<td>$res[3]</td>";
					echo "<td>$res[4]</td>";
					echo "<td width='30%'><a href='#' data-toggle='modal' data-target='#myModal'
					data-id_caso 		= '$id_caso'
					data-id_cliente		= '$id_cliente'
					data-id_area		= '$id_area'
					data-descripcion	= '$descripcion'
					data-fec_inicio		= '$fec_inicio'
					data-fec_final		= '$fec_final'
					data-observaciones	= '$observaciones'
					><img class='img-caso' src='img/edit.png'></a></td>";
				echo "</tr>";
				} 
			?>           
        </tbody>
    </table>
            </div>
        </div>
    </div>

	</div>
	
</form>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
          <h4 class="modal-title">MODIFICACI&Oacute;N CASOS</h4>
        </div>
		
		<div class="modal-body">

		<form action="edit_caso.php" method="post">
			<div class="ocultar">
				<label>ID CASO</label>
				<input type="text" name="id_caso" class="form-control centrar" readonly="">
			</div>
			<div class="ocultar">
				<label>ID CONTACTO</label>
				<input type="text" name="id_contacto" class="form-control centrar" readonly="">
			</div>
			<div class="ocultar">
				<label>ID AREA</label>
				<input type="text" name="id_area" class="form-control centrar" readonly="">
			</div>						
			<div>
	  			<label>DESCRIPCI&Oacute;N DEL CASO</label>
	  			<input type="text" name="descripcion" class="form-control upper" placeholder="Descripci&oacute;n">
		  </div>
		  <div>
	  			<label>FECHA INICIO</label>
				<input type="text" name="fec_inicio" id="fecha1" class="form-control upper centrar" placeholder="Fecha Inico">
		  </div>
		  <div>
	  			<label>FECHA FINAL</label>
				<input type="text" name="fec_final" id="fecha2" class="form-control upper centrar" placeholder="Fecha Final">
		  </div>
		  <div>
	  			<label>OBSERVACIONES</label>
	  			<input type="text" name="observaciones" class="form-control upper" placeholder="Observaciones">
		  </div>

       </div>
		
        <div class="modal-footer">
			<button type="submit" class="boton3">GRABAR</button>
        	<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
        </div>	
		</form>
		</div>	
		
   
    </div>
  </div>
  <script>
	$('#myModal').on('show.bs.modal', function(e)
	{
		var id_caso 	= $(e.relatedTarget).data('id_caso');
		var id_cliente 	= $(e.relatedTarget).data('id_cliente');
		var id_area 	= $(e.relatedTarget).data('id_area');
		var descripcion	= $(e.relatedTarget).data('descripcion');
		var fec_inicio	= $(e.relatedTarget).data('fec_inicio');
		var fec_final	= $(e.relatedTarget).data('fec_final');
		var observaciones	= $(e.relatedTarget).data('observaciones');


		$(e.currentTarget).find('input[name="id_caso"]').val(id_caso);
		$(e.currentTarget).find('input[name="id_contacto"]').val(id_cliente);
		$(e.currentTarget).find('input[name="id_area"]').val(id_area);
		$(e.currentTarget).find('input[name="descripcion"]').val(descripcion);
		$(e.currentTarget).find('input[name="fec_inicio"]').val(fec_inicio);
		$(e.currentTarget).find('input[name="fec_final"]').val(fec_final);
		$(e.currentTarget).find('input[name="observaciones"]').val(observaciones);

	});
  </script>