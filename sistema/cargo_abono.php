<?php
session_start();

$usuario  = $_SESSION['usuario'];
$usuario  = strtoupper($usuario);

require_once('db/conexion.php');



$sql = mysqli_query($conn, "SELECT A.ID_CASO, A.DESCRIPCION
                            FROM tb_caso A,
                               tb_acceso B
                            WHERE A.ID_CASO = B.ID_CASO
                              AND B.ID_USUARIO = '".$usuario."'");

if(isset($_POST['caso']))
    {
      $caso = $_POST['caso'];  
    }
    else{
      $caso = '';        
    }


$detalle = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, DESCRIPCION, MONTO, TIPO
                                  FROM tb_cargo_abono
                                  WHERE TIPO <> 6
                                    AND ID_CASO = '".$caso."'
                                  ORDER BY FECHA ASC");

$sql1 = mysqli_query($conn, "SELECT SALDO
              								FROM tb_corriente
              							  WHERE ID_CASO = '".$caso."'");

while($rowAA = $sql1->fetch_array(MYSQLI_ASSOC)){
	$saldo = $rowAA['SALDO'];
}

if (isset($saldo) <= 0){
	$saldo = "0.00";
}else{
	$saldo = number_format($saldo,2,'.',',');
}

$encabezado = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, MONTO
                  									FROM tb_cargo_abono
                  									WHERE TIPO = 6
                  									  AND ID_CASO = '".$caso."'");

while($rowA1 = $encabezado->fetch_array(MYSQLI_ASSOC)){
	$monto = number_format($rowA1['MONTO'],2,'.',',');
	$fecha = $rowA1['FEC'];
}


if (isset($monto) <= 0){
	$monto = "0.00";
}else{
	$monto = $monto;
}

if (isset($fecha) == null){
	$fecha = "";
}else{
	$fecha = $fecha;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

  </head>
<body>

<div class="container">
      <div class="col-md-12">
      <div class="top-line" style="margin-top: 25px; margin-bottom: 30px;">
            <div class="col-md-4" data-line="movil"><div class="line"></div></div>
            <div class="col-md-4 titulo-seccion"><p>CARGO ABONO</p></div>
            <div class="col-md-4"><div class="line"></div></div>
        </div>        
      </div> 
<div class="col-md-12 baja"> 
  <button type="button" class="boton3" data-toggle="modal" data-target="#myModal">REALIZAR ABONO</button>
</div>

  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
      <div class="modal-header">
          <h4 class="modal-title">ABONOS DE CLIENTES</h4>
          <button type="button" class="close" data-dismiss="modal">Cerrar</button>
        </div>
        
        <div class="modal-body">
          <form action="menu.php?id=6" method="post">
            <div>
              <label>SELECCIONAR NUMERO DE CASO</label>
              <select name="id_caso" class="form-control abono" id="id_caso">
                <option value="-1">SELECCIONAR CASO</option>
            <?php
            while ($row = mysqli_fetch_array($sql))
            {
              echo '<option value="' . $row['ID_CASO']. '">' . $row['DESCRIPCION'] . '</option>' . "\n";
            }
            ?>                  
              </select>
            </div>
            <div>
              <label>DESCRIPCI&Oacute;N</label>
              <input type="text" name="desc_abono" class="form-control abono upper" placeholder="Ingresar Descripci&oacute;n">
            </div>
            <div>
              <label>MONTO</label>
              <input type="text"  name="monto" class="form-control abono upper" placeholder="Ingresar el Monto Abonar" >
            </div>
            <div>
              <label>TIPO DE ABONO</label>
              <select name="tipo" class="form-control abono">
                <option value="6">CARGO INICIAL</option>
                <option value="1">CARGO NORMAL</option>
                <option value="2">ABONO NORMAL</option>
                <option value="3">CHEQUE RECHAZADO</option>
                <option value="4">NOTA DE CREDITO</option>
                <option value="5">NOTA DE DEBITO</option>
              </select>
            </div>
            <div>
              <label>OBSERVACIONES</label>
              <input type="text" name="obs_abono" class="form-control abono upper" placeholder="Ingresar Observaci&oacute;n">
            </div>
            <!--div>
              <label>USUARIO</label>
              <select name="usuario" class="form-control abono">
                <option value=""></option>
              </select>
            </div-->
            <div class="boton-formulario bajar">
              <button type="submit" class="boton3">GRABAR</button>
            </div>
          </form>
        </div>
        
        <!--div class="modal-footer">
          <button type="button" class="boton_close" data-dismiss="modal">Close</button>
        </div-->
        
      </div>
    </div>
  </div>

    <form action="menu.php?id=4" method="post">  
  <div class="">
        <div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>BUSQUEDA POR CASO</p></div>
            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
        </div>     
  </div>
  <div class="container bajar">
    <div class="row">
      <div class="col-md-6">
        <label>SELECCIONAR CASO</label>
        <select name="caso" class="form-control upper" placeholder="SELECCIONAR CASO">
          <option value="-1">SELECCIONAR CASO</option>
            <?php
            $sqlX = mysqli_query($conn, "SELECT A.ID_CASO, A.DESCRIPCION
                                          FROM tb_caso A,
                                             tb_acceso B
                                          WHERE A.ID_CASO = B.ID_CASO
                                            AND B.ID_USUARIO = '".$usuario."'");   
                                                     
            while ($rowx = mysqli_fetch_array($sqlX))
            {
              echo '<option value="' . $rowx['ID_CASO']. '">' . $rowx['DESCRIPCION'] . '</option>' . "\n";
            }
            ?>           
        </select>
      </div>
      <div class="col-md-1" style="margin-top: 40px;">
            <div class="boton-formulario">
              <button type="submit" class="boton3">BUSCAR</button>
            </div>        
      </div>
    </div>
  </div>


      <div class="container bajar">
        <div class="row">
        <div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>DETALLE</p></div>
            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
        </div>
       
    <div class="col-md-12 table-responsive bajar">

    	<table class="display nowrap table table-striped table-bordered" style="width:100%;">
    		<thead>
    			<tr>
    				<td>FECHA</td>
    				<td>DESCRIPCI&Oacute;N</td>
    				<td>MONTO</td>
    			</tr>
    		</thead>
    		<tbody>
          	<tr style="background-color: #5C3B2C;">
        		<td style="color: #fff; font-weight: bold;"><?php echo $fecha; ?></td>
        		<td style="color: #fff; font-weight: bold;">CARGO INICIAL DE CASO:</td>
        		<td style="color: #fff; font-weight: bold;"><?php echo $monto; ?></td>
        	</tr>     			
    		</tbody>
    	</table>

      <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
          <thead>
              <tr>
                  <th class="centrar">FECHA</th>
                  <th class="centrar">DETALLE</th>
                  <th class="centrar">MONTO</th>
              </tr>
          </thead>
          <tbody>
          <?php
      		while ($row = mysqli_fetch_array($detalle)){
        			$tipo 	= $row[3];
    				$plata 	= number_format($row[2],2,'.',',');

	        if(($tipo == 2) or ($tipo == 4) or ($tipo == 5)){
	          $tipo = "<img width='20px;' src='img/menos.png'>";
	        }elseif (($tipo == 1) or ($tipo == 3)) {
	          $tipo = "<img width='20px;' src='img/mas.png'>";
	        }

	        echo "<tr>";
	          echo "<td>$row[0]</td>";
	          echo "<td style='text-align: left;'>$row[1]</td>";
	          echo "<td style='text-align: left; margin-left: 25px;'>$tipo".'  &nbsp; &nbsp; &nbsp; '."$plata</td>";
	        echo "</tr>";
	        } 
      	?>       
        <tr style="background-color: #946047;">
        	<td style="color: #fff; font-weight: bold;">FECHA HOY:  <?php echo date('d/m/Y') ?></td>
        	<td style="color: #fff; font-weight: bold;">SALDO TOTAL:</td>
        	<td style="color: #fff; font-weight: bold;"><?php echo $saldo; ?></td>
        </tr>  
          </tbody>
      </table>
    </div>
        </div>
    </div>      
    </form>
  
</body>
</html>
