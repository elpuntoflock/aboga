<?php
require_once('db/conexion.php');

$sql = mysqli_query($conn, "SELECT ID_CASO, DESCRIPCION
                            FROM tb_caso");

$usuario = mysqli_query($conn, "SELECT ID_USUARIO, CONCAT(NOMBRES,' ',NOMBRE2,' ',APELLIDO1,' ',APELLIDO2)NOMBRE
								FROM tb_usuario");

?>
<div class="container">
	<div class="row">
        <div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>ACCESOS A CASOS</p></div>
            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
        </div>	
		
	<form action="menu.php?id=18" method="post">

		<div class="col-md-12" style="margin-top: 50px;">
			<div class="col-md-1"></div>
			<div class="col-md-4">
				<label>SELECCIONE CASO</label>
				<select name="caso" class="form-control" required="">
		            <?php
		            while ($row = mysqli_fetch_array($sql))
		            {
		            	echo '<option value="' . $row['ID_CASO']. '">' . $row['DESCRIPCION'] . '</option>' . "\n";
		            }
		            ?> 						
				</select>
			</div>
			<div class="col-md-4">
				<label>SELECCIONE USUARIO</label>
				<select name="usuario" class="form-control" required="">
		            <?php
		            while ($rowX = mysqli_fetch_array($usuario))
		            {
		              echo '<option value="' . $rowX['ID_USUARIO']. '">' . $rowX['NOMBRE'] . '</option>' . "\n";
		            }
		            ?> 						
				</select>
			</div>

			<div class="col-md-1" style="margin-top: 40px;">
				<div class="boton-formulario">
          			<button type="submit" class="boton3">ACCESO</button>
        		</div>
			</div>

		</div>

	</form>

	</div>
</div>