<?php
require_once('db/conexion.php');

$caso = $_REQUEST['tmp'];

$detalle = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, DESCRIPCION, MONTO, TIPO
                                  FROM tb_cargo_abono
                                  WHERE TIPO <> 6
                                    AND ID_CASO = '".$caso."'
                                  ORDER BY FECHA ASC");


$sql1 = mysqli_query($conn, "SELECT SALDO
              								FROM tb_corriente
              							  WHERE ID_CASO = '".$caso."'");

while($rowAA = $sql1->fetch_array(MYSQLI_ASSOC)){
	$saldo = $rowAA['SALDO'];
}

if (isset($saldo) <= 0){
	$saldo = "0.00";
}else{
	$saldo = number_format($saldo,2,'.',',');
}

$encabezado = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, MONTO
									FROM tb_cargo_abono
									WHERE TIPO = 6
									  AND ID_CASO = '".$caso."'");

while($rowA1 = $encabezado->fetch_array(MYSQLI_ASSOC)){
	$monto = number_format($rowA1['MONTO'],2,'.',',');
	$fecha = $rowA1['FEC'];
}


if (isset($monto) <= 0){
	$monto = "0.00";
}else{
	$monto = $monto;
}

if (isset($fecha) == null){
	$fecha = "";
}else{
	$fecha = $fecha;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  </head>
<body>

    <form action="menu.php?id=9" method="post">
        
            <div class="boton-formulario">
              <button type="submit" class="boton3">REGRESAR</button>
            </div>        


        <div class="">
              <div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
                  <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
                  <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>DETALLE DE CASO</p></div>
                  <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
              </div>     
        </div>

       
    <div class="col-md-12 table-responsive bajar">

    	<table class="display nowrap table table-striped table-bordered" style="width:100%;">
    		<thead>
    			<tr>
    				<td>FECHA</td>
    				<td>DESCRIPCI&Oacute;N</td>
    				<td>MONTO</td>
    			</tr>
    		</thead>
    		<tbody>
          	<tr style="background-color: #5C3B2C;">
        		<td style="color: #fff; font-weight: bold;"><?php echo $fecha; ?></td>
        		<td style="color: #fff; font-weight: bold;">CARGO INICIAL DE CASO:</td>
        		<td style="color: #fff; font-weight: bold;"><?php echo $monto; ?></td>
        	</tr>     			
    		</tbody>
    	</table>

      <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
          <thead>
              <tr>
                  <th class="centrar">FECHA</th>
                  <th class="centrar">DETALLE</th>
                  <th class="centrar">MONTO</th>
              </tr>
          </thead>
          <tbody>
          <?php
      		while ($row = mysqli_fetch_array($detalle)){
        			$tipo 	= $row[3];
    				$plata 	= number_format($row[2],2,'.',',');

	        if(($tipo == 2) or ($tipo == 4) or ($tipo == 5)){
	          $tipo = "<img width='20px;' src='img/menos.png'>";
	        }elseif (($tipo == 1) or ($tipo == 3)) {
	          $tipo = "<img width='20px;' src='img/mas.png'>";
	        }

	        echo "<tr>";
	          echo "<td>$row[0]</td>";
	          echo "<td style='text-align: left;'>$row[1]</td>";
	          echo "<td style='text-align: left; margin-left: 25px;'>$tipo".'  &nbsp; &nbsp; &nbsp; '."$plata</td>";
	        echo "</tr>";
	        } 
      	?>       
        <tr style="background-color: #946047;">
        	<td style="color: #fff; font-weight: bold;">FECHA HOY:  <?php echo date('d/m/Y') ?></td>
        	<td style="color: #fff; font-weight: bold;">SALDO TOTAL:</td>
        	<td style="color: #fff; font-weight: bold;"><?php echo $saldo; ?></td>
        </tr>  
          </tbody>
      </table>
    </div>
        </div>
    </div>      
    </form>
  
</body>
</html>
