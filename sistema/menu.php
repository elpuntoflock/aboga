<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sistema Abogados</title>

        <!-- CSS -->        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400|Roboto:300,400,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/style_menu.css">

        <!--link rel="stylesheet" href="assets/css/information.css"-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/logo.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="menu/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="menu/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="menu/assets/ico/apple-touch-icon-57-precomposed.png">

        <!--Librerias para Fechas-->
        <link rel="stylesheet" href="assets/css/jquery-ui.css">
        <!--**********************-->

    <link rel="stylesheet" href="assets/css/buttons.dataTables.min.css">
    
    <link rel="stylesheet" type="text/css" href="assets/css/dataTables.bootstrap.min.css">  

    <link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
    <script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>      

    </head>

	<header>
		    <nav class="navbar navbar-fixed-top navbar-no-bg" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!--a class="navbar-brand">Bootstrap Navbar Menu Template</a-->
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="menu.php?id=10">inicio</a></li>
                        <li><a href="menu.php?id=1">Clientes</a></li>
						<li><a href="menu.php?id=2">Casos</a></li>
						<li><a href="menu.php?id=8">Calendario</a></li>
                        <li><a href="menu.php?id=16">Tareas</a></li>
                        <li><a href="menu.php?id=9">Consultas</a></li>
						<li><a href="menu.php?id=4">Cta.Corriente</a></li>
						<li><a href="menu.php?id=17">admin</a></li>
                        <li><a href="index.php">salir</a></li>
					</ul>
				</div>
			</div>
		</nav>
    
    	<!-- Top content -->
        <div class="top-content">
        	<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 text wow fadeInLeft">
						<h1>Jeremy Bentham.</h1>
						<div class="description">
							<p class="medium-paragraph">
                            Los abogados son las únicas personas a quienes la ignorancia de la ley no los castiga.
							</p>
						</div>
					</div>
				</div>
            </div>
        </div>
	</header>

    <body>
    
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					     <?php
           
      if (!isset($_REQUEST['id']) ){ 
          require_once("home.php");


      }
      else 
      {
      $opc = $_REQUEST['id'];
      if ( strlen($opc) >= 0 ) {
          switch ($opc)
      { 
              
      case 1  : require_once("contacto.php"); break;
      case 2  : require_once("casos.php"); break;
      case 3  : require_once("cuenta_corriente.php"); break;
      case 4  : require_once("cargo_abono.php"); break;
      case 5  : require_once("tareas.php"); break;
      case 6  : require_once("insert_cta_corriente.php"); break;
      case 7  : require_once("calendar.php"); break;
      case 8  : require_once("calendarEvent.php"); break;
      case 9  : require_once("consulta.php"); break;
      case 10 : require_once("home.php"); break;
      case 11 : require_once("detalle_saldo.php"); break;
      case 12 : require_once("crea_folder.php"); break;
      case 13 : require_once("borra_folder.php"); break;
      case 14 : require_once("sube_archivo.php"); break;
      case 15 : require_once("add_archivo.php"); break;
      case 16 : require_once("rep_tareas.php"); break;
      case 17 : require_once("adm_accesos.php"); break;
      case 18 : require_once("graba_accesos.php"); break;
      case 19 : require_once("edit_contacto.php"); break;


                                  
      default : echo 'Esta opci&oacute;n no existe'; break;
                }
            }
        }

        ?>  
				</div>
			</div>
		</div>
        <!-- Footer -->
        <!--footer>
	        <div class="container">
	        	<div class="row">
	        		<div class="col-sm-12 footer-copyright">
                    	&copy; Derechos Reservados <?php echo date('Y'); ?>
                    </div>
                </div>
	        </div>
        </footer-->


        <!-- Javascript -->
        <?php 
        if ($_REQUEST['id'] != 8){
           echo "<script src='menu/assets/js/jquery-1.11.1.min.js'></script>";
        }
        ?>

        
    <script src="menu/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="menu/assets/js/jquery.backstretch.js"></script>
    <script src="menu/assets/js/wow.min.js"></script>
        <!-- script src="menu/assets/js/retina-1.1.0.min.js"></script-->
    <script src="menu/assets/js/waypoints.min.js"></script>
    <script src="menu/assets/js/scripts.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

    <script src="assets/js/jquery-ui.js"></script> 

    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/dataTables.buttons.min.js"></script>
    <script src="assets/js/buttons.flash.min.js"></script>
    <script src="assets/js/jszip.min.js"></script>
    <script src="assets/js/pdfmake.min.js"></script>
    <script src="assets/js/vfs_fonts.js"></script>
    <script src="assets/js/buttons.html5.min.js"></script>
    <script src="assets/js/buttons.print.min.js"></script> 

      <script src="assets/js/funcion_select.js"></script>

        <script>
          $( function() {
             $( "#datepicker" ).datepicker();
          } );
          $( function() {
             $( "#datepicker_1" ).datepicker();
          } );
          $( function() {
             $( "#fecha1" ).datepicker();
          } );
          $( function() {
             $( "#fecha2" ).datepicker();
          } );   
          $( function() {
             $( "#fec_notifica" ).datepicker();
          } );                            
        </script>  

<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Blfrtip',
        language:{'url': 'assets/js/spanish.json'},  
        buttons: [
            'excel', 'pdf'
        ]
        
    } );
    } );
    $(document).ready(function() {
    $('#example1').DataTable( {
        dom: 'Blfrtip',
        language:{'url': 'assets/js/spanish.json'},  
        buttons: [
            'excel', 'pdf'
        ]
        
    } );
    } );    
</script> 
    
        
    </body>

</html>